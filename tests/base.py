import unittest
from datetime import date

from investment.models import Loan, Tranche, Investor


class BaseTestCase(unittest.TestCase):

    def setUp(self):
        self.loan = Loan(date(2015, 10, 1), date(2015, 11, 15))

        self.tranche1 = Tranche("A", 1000, 0.03, self.loan)
        self.tranche2 = Tranche("B", 1000, 0.06, self.loan)

        self.investor1 = Investor("Investor1", 1000)
        self.investor2 = Investor("Investor2", 1000)
        self.investor3 = Investor("Investor3", 1000)
        self.investor4 = Investor("Investor4", 1000)
