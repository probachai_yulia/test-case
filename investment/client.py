import logging
from datetime import date

from investment.models import Investor, Tranche, Loan


class Period(object):
    '''
        Period of the report to generate
    '''

    def __init__(self, period_start: date, period_end: date) -> None:
        self.period_start = period_start
        self.period_end = period_end


class Investment:
    '''
        Investment client
    '''

    @classmethod
    def __invest__(cls, investor: Investor, tranche: Tranche, amount: int) -> None:
        # check if the invest sum doesn't exceed tranche's max amount(dynamic check)
        # check if investor has enough money to invest(dynamic check)
        if tranche.is_available_to_invest(amount) and investor.has_money(amount):
            investor.invest(amount)
            tranche.invest(amount)
        else:
            raise ValueError("Unavailable to invest to current tranche or investor has no much money to invest")

    @classmethod
    def get_count_of_days(cls, invest_date: date, loan: Loan, period: Period) -> (int, int):
        '''
        Method calculates number of days for investing with checks
        if the report period contains start date of investments

        :param invest_date: the date of investment
        :param loan: Loan object with dates of start and end
        :param period: Period object with dates of the report
        :return: number of days of the report and number of days of investment
        '''
        start_invest_date = invest_date if invest_date > period.period_start else period.period_start
        end_invest_date = period.period_end if period.period_end < loan.end_date else loan.end_date
        invest_days_count = abs(end_invest_date - start_invest_date).days + 1
        period_days_count = abs(period.period_end - period.period_start).days + 1
        return invest_days_count, period_days_count

    @classmethod
    def calculate_interest(cls, amount: int, tranche: Tranche, date: date, loan: Loan, period: Period) -> float:
        invest_days_count, period_days_count = cls.get_count_of_days(date, loan, period)
        logging.debug("Number of days for reporting: {}".format(period_days_count))
        logging.debug("Number of days of investment: {}".format(invest_days_count))
        return invest_days_count / period_days_count * amount * tranche.interest_rate

    @classmethod
    def perform_invest(cls, tranche: Tranche, investor: Investor, invest_sum: int, date: date, period: Period) -> float:
        '''

        :param tranche: Tranche of Loan
        :param investor: Investor
        :param invest_sum: amount to invest
        :param date: investment date
        :param period: Period of the investment report
        :return: final calculated interest
        '''
        # check if the loan is open(static check)
        if tranche.loan.is_open(date) and invest_sum >= 0:
            logging.debug("Calculating interest for period: {} - {}".format(period.period_start, period.period_end))
            cls.__invest__(investor, tranche, invest_sum)
            interest = cls.calculate_interest(invest_sum, tranche, date, tranche.loan, period)
            logging.debug("{} earns {} from tranche {}".format(investor.name, interest, tranche.name))
            return round(interest, 2)
        raise ValueError("The loan is closed to invest or value to invest is negative number")
