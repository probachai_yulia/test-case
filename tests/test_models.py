import unittest
from datetime import date

from investment.models import Loan, Tranche, Investor
from tests.base import BaseTestCase


class ModelTestCase(BaseTestCase):

    def test_loan(self):
        # the loan is open
        is_open = self.loan.is_open(date(2015, 10, 24))
        self.assertTrue(is_open, True)

        # the loan is closed
        is_open = self.loan.is_open(date(2015, 12, 24))
        self.assertFalse(is_open, False)

    def test_investor(self):
        # investor has 1000 pounds

        # can invest 1000 pounds
        invest_1000_pounds = self.investor1.has_money(1000)
        self.assertTrue(invest_1000_pounds)

        # can't invest 1001 pounds
        invest_1001_pounds = self.investor1.has_money(1001)
        self.assertFalse(invest_1001_pounds, False)

        # invest 500
        balance = self.investor1.invest(500)
        # investor has 500 pounds
        self.assertEqual(balance, 500)

        # try to invest 1000
        balance = self.investor1.invest(1000)
        # has no money to invest, so the balance is still 500
        self.assertEqual(balance, 500)

    def test_tranche(self):
        # tranche max amount is 1000 pounds

        # can invest 1000 pounds
        invest_1000_pounds = self.tranche1.is_available_to_invest(1000)
        self.assertTrue(invest_1000_pounds, True)

        # can't invest 1001 pounds
        invest_1001_pounds = self.tranche1.is_available_to_invest(1001)
        self.assertFalse(invest_1001_pounds, False)

        # invest 700
        amount = self.tranche1.invest(700)
        # tranche amount is 300
        self.assertEqual(amount, 700)

        # try to invest 500
        amount = self.tranche1.invest(500)
        # max amount is reached,  investments can't be made
        self.assertEqual(amount, 700)

    def test_models_validation(self):
        # test Loan invalid date
        with self.assertRaises(Exception) as context:
            loan = Loan('2015/13/1', date(2015, 11, 15))
            self.assertTrue('start_date must be a date' in context.exception)

        # test investor's invalid name
        with self.assertRaises(Exception) as context:
            investor1 = Investor(111, 1000)
            self.assertTrue('name must be a string' in context.exception)

        # test investor's invalid wallet balance
        with self.assertRaises(Exception) as context:
            investor2 = Investor('blah', '1000')
            self.assertTrue('wallet_balance must be int' in context.exception)

        # test trance's invalid max_amount
        with self.assertRaises(Exception) as context:
            trance1 = Tranche("A", '1000', 0.03, self.loan)
            self.assertTrue('max_amount must be int' in context.exception)

        # test trance's invalid interest_rate
        with self.assertRaises(Exception) as context:
            trance2 = Tranche("A", 1000, 3, self.loan)
            self.assertTrue('interest_rate must be a float' in context.exception)

        # test trance's invalid loan
        with self.assertRaises(Exception) as context:
            trance3 = Tranche("A", 1000, 3, trance2)
            self.assertTrue("loan must be a Loan type" in context.exception)


if __name__ == '__main__':
    unittest.main()
