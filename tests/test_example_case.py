from datetime import date

from investment.client import Investment, Period
from tests.base import BaseTestCase


class StandardTestCase(BaseTestCase):
    period = Period(date(2015, 10, 1), date(2015, 10, 31))

    def test_investment(self):
        # test investor 1
        sum = Investment.perform_invest(self.tranche1, self.investor1, 1000, date(2015, 10, 3), self.period)
        self.assertEqual(sum, 28.06)

        # test investor 2
        with self.assertRaises(ValueError) as context:
            Investment.perform_invest(self.tranche1, self.investor2, 1, date(2015, 10, 4), self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)
        # test investor 3
        sum = Investment.perform_invest(self.tranche2, self.investor3, 500, date(2015, 10, 10), self.period)
        self.assertEqual(sum, 21.29)

        # test investor 4
        with self.assertRaises(ValueError) as context:
            Investment.perform_invest(self.tranche2, self.investor4, 1100, date(2015, 10, 25), self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)
