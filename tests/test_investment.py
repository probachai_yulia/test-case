import unittest
from datetime import date

from investment.client import Period, Investment
from investment.models import Loan, Tranche, Investor


class InvestmentTestCase(unittest.TestCase):

    def setUp(self):
        self.period = Period(date(2017, 12, 1), date(2017, 12, 31))

        self.loan = Loan(date(2017, 5, 14), date(2018, 4, 7))

        self.tranche1 = Tranche("A", 1000, 0.04, self.loan)
        self.tranche2 = Tranche("B", 2000, 0.06, self.loan)

        self.investor1 = Investor("Investor1", 1000)
        self.investor2 = Investor("Investor2", 500)
        self.investor3 = Investor("Investor3", 2000)
        self.investor4 = Investor("Investor4", 1000)

    def test_investor_money(self):
        # test investor 1
        # investor spend all money
        investment1 = Investment.perform_invest(self.tranche1, self.investor1, 500, date(2017, 12, 3), self.period)
        self.assertEqual(investment1, 18.71)
        self.assertEqual(self.tranche1.current_amount, 500)
        self.assertEqual(self.investor1.wallet_balance, 500)
        self.assertTrue(self.tranche1.is_available_to_invest())
        self.assertTrue(self.investor1.has_money())

        investment2 = Investment.perform_invest(self.tranche2, self.investor1, 500, date(2017, 12, 17), self.period)
        self.assertEqual(investment2, 14.52)
        self.assertEqual(self.tranche2.current_amount, 500)
        self.assertEqual(self.investor1.wallet_balance, 0)
        self.assertTrue(self.tranche2.is_available_to_invest())
        self.assertFalse(self.investor1.has_money())

        with self.assertRaises(ValueError) as context:
            investment3 = Investment.perform_invest(self.tranche1, self.investor1, 500, date(2017, 12, 1), self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)

    def test_tranche_max_amount(self):
        investment1 = Investment.perform_invest(self.tranche1, self.investor2, 500, date(2017, 12, 12), self.period)
        self.assertEqual(investment1, 12.90)
        self.assertEqual(self.tranche1.current_amount, 500)
        self.assertEqual(self.investor2.wallet_balance, 0)
        self.assertTrue(self.tranche1.is_available_to_invest())
        self.assertFalse(self.investor2.has_money())

        # tranche's max amout will be reached in the next investment
        with self.assertRaises(ValueError) as context:
            investment2 = Investment.perform_invest(self.tranche1, self.investor3, 700, date(2017, 12, 17), self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)

        investment3 = Investment.perform_invest(self.tranche1, self.investor3, 500, date(2017, 12, 11), self.period)
        self.assertEqual(investment3, 13.55)
        self.assertEqual(self.tranche1.current_amount, 1000)
        self.assertEqual(self.investor3.wallet_balance, 1500)
        self.assertFalse(self.tranche1.is_available_to_invest())
        self.assertTrue(self.investor3.has_money())

        # tranche's max amount is reached
        with self.assertRaises(ValueError) as context:
            investment4 = Investment.perform_invest(self.tranche1, self.investor3, 500, date(2017, 12, 1), self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)

    def test_loan_period(self):
        # invest after close date
        with self.assertRaises(ValueError) as context:
            investment1 = Investment.perform_invest(self.tranche1, self.investor2, 500, date(2018, 12, 12), self.period)
            self.assertTrue("The loan is closed to invest" in context.exception)

        # invest before open date
        with self.assertRaises(ValueError) as context:
            investment2 = Investment.perform_invest(self.tranche2, self.investor2, 500, date(2017, 2, 12), self.period)
            self.assertTrue("The loan is closed to invest" in context.exception)

        # invest before start date of the report
        # interest should be counted for '01/12/2017 - 31/12/2017' - report period
        investment3 = Investment.perform_invest(self.tranche2, self.investor3, 1000, date(2017, 11, 11), self.period)
        self.assertEqual(investment3, 60.00)
        self.assertEqual(self.tranche2.current_amount, 1000)
        self.assertEqual(self.investor3.wallet_balance, 1000)
        self.assertTrue(self.tranche2.is_available_to_invest())
        self.assertTrue(self.investor3.has_money())

        # loan ends before end date of the report
        # interest should be counted for '01/4/2018 - 7/4/2018' - report period
        period = Period(date(2018, 4, 1), date(2018, 4, 30))
        investment4 = Investment.perform_invest(self.tranche1, self.investor1, 1000, date(2017, 12, 3), period)
        self.assertEqual(investment4, 9.33)
        self.assertEqual(self.tranche1.current_amount, 1000)
        self.assertEqual(self.investor1.wallet_balance, 0)
        self.assertFalse(self.tranche1.is_available_to_invest())
        self.assertFalse(self.investor1.has_money())

        # invest in the last day of the forming report
        # interest should be counted for '31/12/2017 - 31/12/2017' - report period
        investment5 = Investment.perform_invest(self.tranche2, self.investor4, 1000, date(2017, 12, 31), self.period)
        self.assertEqual(investment5, 1.94)
        self.assertEqual(self.tranche2.current_amount, 2000)
        self.assertEqual(self.investor4.wallet_balance, 0)
        self.assertFalse(self.tranche2.is_available_to_invest())
        self.assertFalse(self.investor4.has_money())

    def test_negatives(self):
        # test negative values

        investor = Investor("Investor1", -1000)
        tranche = Tranche("A", -1000, 0.04, self.loan)

        # tranche has negative max value
        with self.assertRaises(ValueError) as context:
            investment1 = Investment.perform_invest(tranche, self.investor1, 700, date(2017, 12, 17), self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)
        self.assertFalse(tranche.is_available_to_invest())
        self.assertTrue(self.investor1.has_money())

        # investor has negative wallet sum
        with self.assertRaises(ValueError) as context:
            investment2 = Investment.perform_invest(self.tranche1, investor, 700, date(2017, 12, 17), self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)
        self.assertTrue(self.tranche1.is_available_to_invest())
        self.assertFalse(investor.has_money())

        # negative value to invest
        with self.assertRaises(ValueError) as context:
            investment3 = Investment.perform_invest(self.tranche2, self.investor4, -500, date(2017, 12, 17),
                                                    self.period)
            self.assertTrue("Unavailable to invest to current tranche" in context.exception)
        self.assertTrue(self.tranche2.is_available_to_invest())
        self.assertTrue(self.investor4.has_money())


if __name__ == '__main__':
    unittest.main()
