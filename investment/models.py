import logging
from datetime import date


class Loan(object):

    def __init__(self, start_date: date, end_date: date) -> None:
        '''

        :param start_date: start date of loan
        :param end_date: end date of loan
        '''
        self.start_date = start_date
        self.end_date = end_date

    def is_open(self, current_date: date) -> bool:
        '''

        :param current_date:
        :return: boolean
        if the loan is available to invest in certain date
        '''
        logging.debug("Loan is available for the date {}".format(current_date))
        return self.start_date <= current_date <= self.end_date


class Tranche(object):

    def __init__(self, name: str, max_amount: int, interest_rate: float, loan: Loan, current_amount: int = 0) -> None:
        '''

        :param name: tranche name
        :param max_amount: max amount in the tranche
        :param interest_rate: interest rate of the tranche
        :param loan: the loan includes the tranche
        :param current_amount: current amount of the tranche
        '''
        self.name = name
        self.max_amount = max_amount
        self.interest_rate = interest_rate
        self.loan = loan
        self.current_amount = current_amount

    def is_available_to_invest(self, investment: int = 0) -> bool:
        '''
        checks if the tranche is available to invest
        :param investment:
        :return:
        '''
        if (self.current_amount == self.max_amount) or (self.current_amount + investment > self.max_amount):
            return False
        logging.debug("Tranche is available to invest {} pounds".format(investment))
        return True

    def invest(self, amount: int) -> int:
        '''
        invest to tranche
        :param amount:
        :return:
        '''
        if self.is_available_to_invest(amount):
            logging.debug("Tranche amount before investment: {}".format(self.current_amount))
            self.current_amount = self.current_amount + amount
            logging.debug("Amount of investment: {}".format(amount))
            logging.debug("Final tranche amount: {}".format(self.current_amount))
        return self.current_amount


class Investor(object):

    def __init__(self, name: str, wallet_balance: int) -> None:
        '''
        :param name: investor name
        :param wallet_balance: balance of wallet if the investor
        '''
        self.name = name
        self.wallet_balance = wallet_balance

    def has_money(self, value: int = 0) -> bool:
        '''
        checks if the investor has enough money to invest
        :param value:
        :return:
        '''
        if (self.wallet_balance == 0) or (self.wallet_balance - value < 0):
            return False
        logging.debug("Investor has {} pounds in the wallet to invest".format(value))
        return True

    def invest(self, amount: int) -> int:
        '''
        invests money if available
        :param amount:
        :return:
        '''
        if self.has_money(amount):
            logging.debug("Investor wallet balance before investment: {}".format(self.wallet_balance))
            self.wallet_balance = self.wallet_balance - amount
            logging.debug("Amount of investment: {}".format(amount))
            logging.debug("Final wallet balance: {}".format(self.wallet_balance))
        return self.wallet_balance
